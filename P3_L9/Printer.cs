﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P3_L9
{
    public class Printer
    {
        private bool _isOk;

        private int _paperCount;

        public List<Ink> Inks;

        public event EventHandler OutOfPaperEvent;

        public event EventHandler<Color> OutOfInkEvent;



        private void OutOfPaperEventHandler(object sender, EventArgs args)
        {
            Console.WriteLine($"[Printer log] Out of Paper  {DateTime.Now.ToLocalTime()}");
        }

        private void OutOfInkEventHandler(object sender, Color color)
        {
            Console.WriteLine($"[Printer log] Out of {color} Ink ");
        }

        public Printer(int paperCount) : this()
        {
            this._paperCount = paperCount;

            this.Inks = new List<Ink>
                        {
                            new Ink(Color.Red),
                            new Ink(Color.Green),
                            new Ink(Color.Blue),
                            new Ink(Color.Black)
                        };
        }

        public Printer()
        {
            OutOfPaperEvent += OutOfPaperEventHandler;
            OutOfInkEvent += OutOfInkEventHandler;
        }

        public void Print(int numOfPages)
        {
            for (int i = 0; i < numOfPages; i++)
            {
                if (this._paperCount > 0)
                {
                    foreach (var ink in this.Inks)
                    {
                        if (ink.Level < 0)
                        {
                            OutOfInkEvent?.Invoke(this, ink.Color);
                           // return;
                        }
                        ink.Level -= new Random().Next(1, 3);
                    }
                    Console.WriteLine("Printing...");
                    --this._paperCount;
                }
                else
                {
                    OutOfPaperEvent?.Invoke(this, EventArgs.Empty);
                    return;
                }
            }
        }

    }
}
