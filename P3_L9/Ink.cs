﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P3_L9
{
    public class Ink
    {
        public Color Color { get; set; }
        public int Level { get; set; }

        public Ink(Color color)
        {
            this.Color = color;
            this.Level = 100;
        }
    }
}
